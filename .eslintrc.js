module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: 'standard',
  overrides: [
    {
      env: {
        node: true
      },
      files: [
        '.eslintrc.{js,cjs}'
      ],
      parserOptions: {
        sourceType: 'script'
      }
    }
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  rules: {
    "no-unused-vars": ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
    "no-await-in-loop": "error",
    "semi": ["error", "always"],
    "quotes": ["error", "double"],
    "no-unreachable-loop": ["error", { "ignore": ["ForInStatement", "ForOfStatement"] }],
    "valid-typeof": ["error", { "requireStringLiterals": true }],
    "use-isnan": ["error", {"enforceForIndexOf": true}],
    "no-self-assign": ["error", {"props": true}],
    "no-irregular-whitespace": ["error", { "skipComments": true }],
    "no-unused-private-class-members": "error"
  }
}
